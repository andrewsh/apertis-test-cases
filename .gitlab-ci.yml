stages:
  - test
  - generate

variables:
  osname: apertis
  release: v2025dev2

image: $CI_REGISTRY/infrastructure/${osname}-docker-images/${release}-testcases-builder

include:
  - /.gitlab-ci/templates-generate-tests.yml

test-renderer:
  stage: test
  tags:
    - lightweight
  script:
    - python3 -m unittest discover -v

render-pages:
  stage: test
  tags:
    - lightweight
  script:
    - ./atc -d ${release}/ --index-page test-cases/
  artifacts:
    paths:
      - ${release}

.fixedfunction:
  variables:
    type: fixedfunction

.hmi:
  variables:
    type: hmi

.basesdk:
  variables:
    type: basesdk

.sdk:
  variables:
    type: sdk

.amd64-uefi:
  variables:
    architecture: amd64
    board: uefi

.amd64-sdk:
  variables:
    architecture: amd64
    board: sdk

.armhf-uboot:
  variables:
    architecture: armhf
    board: uboot

.arm64-uboot:
  variables:
    architecture: arm64
    board: uboot

.arm64-rpi64:
  variables:
    architecture: arm64
    board: rpi64

.generate-tests-apt:
  extends: .generate-tests
  tags:
    - lightweight
  stage: generate
  variables:
    deployment: apt
    image_name: ${osname}_${release}-${type}-${architecture}-${board}
    osname: apertis
    release: v2025dev2
    base_url: https://images.apertis.org
    image_path: weekly/v2025dev2
    image_buildid: "fakebuildid"

.generate-tests-ostree:
  extends: .generate-tests
  tags:
    - lightweight
  stage: generate
  variables:
    deployment: ostree
    image_name: ${osname}_ostree_${release}-${type}-${architecture}-${board}
    image_bundle: ${osname}_ostree_${release}-${type}-${architecture}-${board}
    osname: apertis
    release: v2025dev2
    base_url: https://images.apertis.org
    image_path: weekly/v2025dev2
    image_buildid: "fakebuildid"

# !!!
# All generate-tests-* jobs defined below are not really used,
# they are intended to test that the jobs can be generated.
# !!!

generate-tests-apt-amd64-fixedfunction-uefi:
  extends:
    - .generate-tests-apt
    - .amd64-uefi
    - .fixedfunction

generate-tests-apt-armhf-fixedfunction-uboot:
  extends:
    - .generate-tests-apt
    - .armhf-uboot
    - .fixedfunction

generate-tests-apt-arm64-fixedfunction-uboot:
  extends:
    - .generate-tests-apt
    - .arm64-uboot
    - .fixedfunction

generate-tests-apt-arm64-fixedfunction-rpi64-rpi4:
  extends:
    - .generate-tests-apt
    - .arm64-rpi64
    - .fixedfunction

generate-tests-apt-amd64-hmi-uefi:
  extends:
    - .generate-tests-apt
    - .amd64-uefi
    - .hmi

generate-tests-apt-armhf-hmi-uboot:
  extends:
    - .generate-tests-apt
    - .armhf-uboot
    - .hmi

generate-tests-apt-arm64-hmi-rpi64-rpi4:
  extends:
    - .generate-tests-apt
    - .arm64-rpi64
    - .hmi

generate-tests-apt-amd64-basesdk-sdk:
  extends:
    - .generate-tests-apt
    - .amd64-sdk
    - .basesdk

generate-tests-apt-amd64-sdk-sdk:
  extends:
    - .generate-tests-apt
    - .amd64-sdk
    - .sdk

generate-tests-ostree-amd64-fixedfunction-uefi:
  extends:
    - .generate-tests-ostree
    - .amd64-uefi
    - .fixedfunction

generate-tests-ostree-armhf-fixedfunction-uboot:
  extends:
    - .generate-tests-ostree
    - .armhf-uboot
    - .fixedfunction

generate-tests-ostree-arm64-fixedfunction-uboot:
  extends:
    - .generate-tests-ostree
    - .arm64-uboot
    - .fixedfunction

generate-tests-ostree-arm64-fixedfunction-rpi64-rpi4:
  extends:
    - .generate-tests-ostree
    - .arm64-rpi64
    - .fixedfunction
  variables:
    image_bundle: ${osname}_ostree_${release}-${type}-${architecture}-uboot

generate-tests-ostree-amd64-hmi-uefi:
  extends:
    - .generate-tests-ostree
    - .amd64-uefi
    - .hmi

generate-tests-ostree-arm64-hmi-rpi64-rpi4:
  extends:
    - .generate-tests-ostree
    - .arm64-rpi64
    - .hmi
  variables:
    image_bundle: ${osname}_ostree_${release}-${type}-${architecture}-uboot
