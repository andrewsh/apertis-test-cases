#!/usr/bin/python3
###################################################################################
# Apertis LAVA Job description generator
# Copyright (C) 2023 Collabora Ltd
# Dylan Aïssi <dylan.aissi@collabora.com>

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
###################################################################################

import argparse
import os
import subprocess
import sys
import yaml

parent_pipeline = os.environ.get("CI_PIPELINE_ID")
image_pipeline = {}

def generate_image_job(release, arch, board, osname, image_type, image_buildid, image_name, bin_pkg_list):
    variables = {
        'RELEASE': release,
        'ARCH': arch,
        'BOARD': board,
        'OSNAME': osname,
        'TYPE': image_type,
        'BUILDID': image_buildid,
        'IMAGE_NAME': image_name,
        'BIN_PKG_LIST': bin_pkg_list,
    }
    image_pipeline[f'build-image-job-{image_name}-package-centric'] = {
        'variables': variables,
        'extends': '.image-job-base',
        'needs': [{
            'pipeline': parent_pipeline,
            'job': 'create-apt-repo',
            'artifacts': True,
        }]
    }


def run_generate_jobs(test_meta, args, image_type, arch, board=None):

    if board is None:
        if arch == "amd64":
            if image_type in ["basesdk", "sdk"]:
                board = "sdk"
            elif image_type in ["fixedfunction", "hmi"]:
                board = "uefi"
        elif arch in ["armhf", "arm64"]:
            board = "uboot"

    osname = args.osname
    release = args.release
    if "image_buildid" in test_meta:
        image_buildid = test_meta["image_buildid"]
    else:
        image_buildid = args.image_id
    image_name = f'{osname}_{release}-{image_type}-{arch}-{board}_{image_buildid}'

    bin_pkg_list = test_meta["bin_pkg_list"].replace("\n", "")
    pkg_test_cmd = test_meta["pkg_test_cmd"]

    gen_jobs = ['./generate-jobs.py']
    gen_jobs.extend(['-d', 'lava/devices.yaml',
                     '--config', 'lava/config.yaml',
                     '--release', release,
                     '--arch', arch,
                     '--board', board,
                     '--osname', osname,
                     '--type', image_type,
                     '--date', image_buildid,
                     '--deployment', 'apt',
                     '--name', image_name,
                     '--test-type', 'package',
                     '--metadata-file', args.output_dir + '/metadata.json',
                     '--output-dir', args.output_dir,
                     '-t', f'visibility:{args.visibility}',
                     '-t', f'BIN_PKG_LIST:{bin_pkg_list}',
                     '-t', f'PKG_TEST_CMD:{pkg_test_cmd}',
                     ])

    if test_meta.get("build-custom-image"):
        gen_jobs.extend(['-t', 'image_base_url:{{job.BUILT_IMAGE_URL}}'])
        gen_jobs.extend(['-t', 'image_needs_job_token:true'])
        generate_image_job(release, arch, board, osname, image_type, image_buildid, image_name, bin_pkg_list)

    subprocess.run(gen_jobs)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', type=str, required=True,
                        help='set the config yaml file (e.g. package-centric-testing.yaml)')
    parser.add_argument('--osname', type=str, default='apertis',
                        help='the OS name for the distribution field in the changelog')
    parser.add_argument('--release', type=str, required=True,
                        help='os release (e.g. v2025dev1)')
    parser.add_argument('--image-id', type=str,
                        help='image id to be used (20231211.0015)')
    parser.add_argument('--visibility', type=str, required=True,
                        help='visibility of LAVA jobs')
    parser.add_argument('--output-image-pipeline', type=argparse.FileType("w"), metavar="OUTPUT",
                        help="the image build pipeline filename to produce.")
    parser.add_argument('--output-dir', type=str,
                        help="where to save the generated jobs", default=".")
    parser.add_argument('--parent-pipeline', type=str, metavar="PARENT",
                        help="""the id of the Gitlab pipeline that will
                                trigger the image pipeline (defaults to
                                $CI_PIPELINE_ID)""")

    args = parser.parse_args()

    try:
        with open(args.config) as f:
            data  = yaml.safe_load(f)
    except yaml.scanner.ScannerError as e:
            logger.error(e)

    if "tests" not in data:
        print(f'Config file does not contain "tests"', file=sys.stderr)
        sys.exit(1)

    if args.parent_pipeline:
        global parent_pipeline
        parent_pipeline = args.parent_pipeline

    test_meta = data["tests"]
    image_types = test_meta.get('image-types')

    for image_type in image_types:
        for arch in image_types[image_type]:
            if isinstance(arch, str):
                run_generate_jobs(test_meta, args, image_type, arch)
            elif isinstance(arch, dict):
                for arch_i, boards in arch.items():
                    for board_i in boards:
                        run_generate_jobs(test_meta, args, image_type, arch_i, board_i)

    if args.output_image_pipeline and image_pipeline:
        args.output_image_pipeline.write(yaml.dump(image_pipeline))


if __name__ == '__main__':
    main()
