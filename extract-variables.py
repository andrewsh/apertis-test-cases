#!/usr/bin/python3
# SPDX-License-Identifier: CC0-1.0

import sys
import yaml

d = yaml.safe_load(sys.stdin)

# walk the dict path
for k in sys.argv[1:]:
    d = d.get(k, {})

# repr could work better than str, but it uses ' instead
# of " which blocks variable transclusion
print("\n".join(
    k + '="' + str(v) + '"'
    for k, v in d.get("variables", {}).items()
))
